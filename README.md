# ttorrent-search

This is a simple GUI for [Transdroid torrent search](https://github.com/erickok/transdroid-search),
to aid search with [tTorrent](https://ttorrent.org), or with an arbitary torrent client.

## Usage
The app will prompt you to install transdroid-search if not installed yet. You need to set up the search plugin, select the sites you want to use, and you are ready to go. The UI is simple, you can enter the search term, and browse the results. Tapping on a result will bring app the add torrent/magnet screen. Deleting the search history is also possible.

### tTorrent set up

In tTorrent settings under Search enable "Use custom app", and pick the tTorrent-search application.

## Contributors
- tagsoft
- Atrate

## License

This project is licensed under the GNU General Public License v3.0



