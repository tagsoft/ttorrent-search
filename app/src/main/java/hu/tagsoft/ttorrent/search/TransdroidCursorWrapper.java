package hu.tagsoft.ttorrent.search;

import android.database.Cursor;
import android.util.Log;

import java.sql.Date;
import java.text.DateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TransdroidCursorWrapper {

    private final static String TAG = "TransdroidCursorWrapper";

    private final Cursor cursor;

    private final String NAME_COLUMN = "NAME";
    private final String TORRENTURL_COLUMN = "TORRENTURL";
    private final String DETAILSURL_COLUMN = "DETAILSURL";
    private final String SIZE_COLUMN = "SIZE";
    private final String ADDED_COLUMN = "ADDED";
    private final String SEEDERS_COLUMN = "SEEDERS";
    private final String LEECHERS_COLUMN = "LEECHERS";

    public TransdroidCursorWrapper(final Cursor cursor) {
        this.cursor = cursor;
    }

    public String getName() {
        return cursor.getString(cursor.getColumnIndex(NAME_COLUMN));
    }

    public String getUrl() {
        return cursor.getString(cursor.getColumnIndex(TORRENTURL_COLUMN));
    }

    public String getDetailsUrl() {

        return cursor.getString(cursor.getColumnIndex(DETAILSURL_COLUMN));
    }

    public long getSize() {
        String raw = cursor.getString(cursor.getColumnIndex(SIZE_COLUMN))
                .toLowerCase();

        long multiplier = 1;

        if (raw.matches("([0-9\\. ]+)(gb|gigabyte|gib)")) {
            multiplier = 1024 * 1024 * 1024;
        } else if (raw.matches("([0-9\\. ]+)(mb|megabyte|mib)")) {
            multiplier = 1024 * 1024;
        } else if (raw.matches("([0-9\\. ]+)(kb|kilobyte|kib)")) {
            multiplier = 1024;
        }

        try {
            Pattern numberPattern = Pattern.compile("([0-9\\. ]+).*");
            Matcher matcher = numberPattern.matcher(raw);
            if (matcher.find()) {
                return (long) Math.floor(Float.parseFloat(matcher.group(1))
                        * multiplier);
            }
            return 0;
        } catch (NumberFormatException ex) {
            Log.d(TAG, ex.toString());
            return 0;
        }
    }

    public String getAdded() {
        String addedColumn = cursor.getString(cursor
                .getColumnIndex(ADDED_COLUMN));
        if (addedColumn == "-1")
            return "";
        try {
            long date = Long.parseLong(addedColumn);
            return DateFormat.getDateInstance(DateFormat.MEDIUM).format(
                    new Date(date));
        } catch (NumberFormatException ex) {
            return addedColumn;
        }
    }

    public int getSeeders() {
        try {
            return Integer.parseInt(cursor.getString(cursor
                    .getColumnIndex(SEEDERS_COLUMN)));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public int getLeechers() {
        try {
            return Integer.parseInt(cursor.getString(cursor
                    .getColumnIndex(LEECHERS_COLUMN)));
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

}
