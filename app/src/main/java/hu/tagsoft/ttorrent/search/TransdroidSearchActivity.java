package hu.tagsoft.ttorrent.search;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TransdroidSearchActivity extends AppCompatActivity {

    ListView listView;
    ProgressBar progressBar;
    TextView emptyTextView;

    Spinner spinner;

    private String queryText;

    private SearchSiteAdapter siteAdapter;

    private String selectedSiteKey;
    private SearchTask searchTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDefaultKeyMode(DEFAULT_KEYS_SEARCH_LOCAL);
        setContentView(R.layout.activity_search);

        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = findViewById(R.id.search_list_view);
        progressBar = findViewById(R.id.search_list_progress);
        emptyTextView = findViewById(R.id.search_list_empty_view);

        siteAdapter = new SearchSiteAdapter(this);
        spinner = findViewById(R.id.search_sites);
        spinner.setAdapter(siteAdapter);
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int itemPosition, long itemId) {
                selectedSiteKey = siteAdapter.getItem(itemPosition).getKey();
                PreferenceManager.getDefaultSharedPreferences(TransdroidSearchActivity.this)
                        .edit()
                        .putString("SEARCH_ENGINE", selectedSiteKey)
                        .apply();
                startSearchTask();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        registerForContextMenu(listView);
        listView.setOnItemClickListener((parent, view, position, id) -> {
            TransdroidCursorWrapper wrapper = new TransdroidCursorWrapper(
                    (Cursor) listView.getItemAtPosition(position));
            addTorrentUrl(wrapper.getUrl());
        });

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo si = searchManager.getSearchableInfo(getComponentName());
        final SearchView sv = findViewById(R.id.search_view);
        sv.setSearchableInfo(si);
        sv.setQueryRefinementEnabled(true);

        if (!TransdroidSearchInstaller
                .checkInstalledTransdroidVersion(this)) {

            TransdroidSearchInstaller.showTransdroidSearchNotInstalled(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateSearchEngines();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.search_menu_clear_history: {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_clear_history_title)
                        .setMessage(R.string.dialog_clear_history_message)
                        .setPositiveButton(R.string.dialog_button_yes,
                                (dialog, which) -> {

                                    SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
                                            TransdroidSearchActivity.this,
                                            TransdroidSearchSuggestionProvider.AUTHORITY,
                                            TransdroidSearchSuggestionProvider.MODE);
                                    suggestions.clearHistory();
                                })
                        .setNegativeButton(R.string.dialog_button_no, null).show();

                return true;
            }
            case R.id.search_menu_settings: {
                Intent intent = getPackageManager().getLaunchIntentForPackage("org.transdroid.search");
                startActivity(intent);
                return true;
            }
        }
        return false;
    }

    @Override
    public void onCreateContextMenu(ContextMenu conMenu, View view,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(conMenu, view, menuInfo);
        if (view.getId() == R.id.search_list_view) {
            AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
            TransdroidCursorWrapper wrapper = new TransdroidCursorWrapper(
                    (Cursor) listView.getItemAtPosition(info.position));

            getMenuInflater().inflate(R.menu.search_context_menu, conMenu);
            conMenu.setHeaderTitle(wrapper.getName());
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
                .getMenuInfo();
        TransdroidCursorWrapper wrapper = new TransdroidCursorWrapper(
                (Cursor) listView.getItemAtPosition(info.position));

        String TAG = "SearchActivity";
        switch (item.getItemId()) {

            case R.id.context_details:
                try {
                    Intent viewIntent = new Intent("android.intent.action.VIEW",
                            Uri.parse(wrapper.getDetailsUrl()));
                    startActivity(viewIntent);
                } catch (ActivityNotFoundException ex) {
                    Log.e(TAG, ex.toString());
                } catch (NullPointerException ex) {
                    Log.e(TAG, ex.toString());
                }
                return true;
            case R.id.context_add:
                addTorrentUrl(wrapper.getUrl());
                return true;
        }
        return false;
    }

    private void updateSearchEngines() {
        AdapterView.OnItemSelectedListener onItemSelectedListener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        siteAdapter.clear();
        Uri uri = Uri
                .parse("content://org.transdroid.search.torrentsitesprovider/sites");
        Cursor sites = managedQuery(uri, null, null, null, null);

        if (sites == null)
            return;

        while (sites.moveToNext()) {
            siteAdapter
                    .add(new SearchSite(sites.getInt(0), sites.getString(1),
                            sites.getString(2),
                            sites.getColumnNames().length > 4 && sites
                                    .getInt(4) >= 1));
        }

        if (siteAdapter.getCount() == 0) {
            Intent intent = getPackageManager().getLaunchIntentForPackage("org.transdroid.search");
            startActivity(intent);
            return;
        }

        selectedSiteKey = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("SEARCH_ENGINE", "Rarbg");

        if (siteAdapter.getSitePosition(selectedSiteKey) < 0)
            selectedSiteKey = siteAdapter.getItem(0).getKey();

        spinner.setSelection(siteAdapter.getSitePosition(selectedSiteKey));
        spinner.setOnItemSelectedListener(onItemSelectedListener);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            queryText = intent.getStringExtra(SearchManager.QUERY);
            final SearchView sv = findViewById(R.id.search_view);
            sv.setQuery(queryText, false);
            sv.clearFocus();
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(
                    this, TransdroidSearchSuggestionProvider.AUTHORITY,
                    TransdroidSearchSuggestionProvider.MODE);
            suggestions.saveRecentQuery(queryText, null);

            startSearchTask();
        }
    }

    private void startSearchTask() {
        if (queryText == null)
            return;
        if (searchTask != null)
            searchTask.cancel(false);
        searchTask = new SearchTask();
        searchTask.execute(queryText);
    }

    private void addTorrentUrl(String url) {
        try {
            Uri torrentUri = Uri.parse(url);

            if (siteAdapter.getItem(
                    siteAdapter.getSitePosition(selectedSiteKey)).isPrivate()) {

                Uri contentUri = Uri.parse("content://org.transdroid.search.torrentsearchprovider/get/" + selectedSiteKey + "/"
                        + URLEncoder.encode(url, "UTF-8"));
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setDataAndType(contentUri, "application/x-bittorrent");
                startActivity(i);

            } else if (torrentUri.getScheme().equalsIgnoreCase("http")
                    || torrentUri.getScheme().equalsIgnoreCase("https")) {
                startAddTorrentActivity(Uri.parse(url));
            } else if (torrentUri.getScheme().equalsIgnoreCase("magnet")) {
                startAddMagnetActivity(Uri.parse(url));
            }
        } catch (ActivityNotFoundException | NullPointerException | UnsupportedEncodingException ex) {
            Log.e("addTorrentUrl", ex.toString());
        }
    }

    private class SearchTask extends AsyncTask<String, Void, Cursor> {

        @Override
        protected void onPreExecute() {
            listView.setAdapter(null);
            progressBar.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.GONE);
        }

        @Override
        protected Cursor doInBackground(String... params) {
            Uri uri = Uri
                    .parse("content://org.transdroid.search.torrentsearchprovider/search/"
                            + params[0]);

            String site = selectedSiteKey;

            if (selectedSiteKey == null)
                return null;

            return managedQuery(uri, null, "SITE = ?", new String[]{site},
                    "BySeeders");
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            progressBar.setVisibility(View.GONE);

            if (cursor == null || cursor.isClosed() || cursor.getCount() == 0) {
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText(queryText == null ? getString(R.string.no_results) : getString(R.string.no_results_for, queryText));
            } else {
                listView.setAdapter(new TransdroidCursorAdapter(
                        TransdroidSearchActivity.this, cursor));
            }
        }

        @Override
        protected void onCancelled(Cursor cursor) {
            super.onCancelled(cursor);
        }


    }

    private void startAddTorrentActivity(Uri uri) {
        if (tryStartSpecificActivity(uri, "hu.tagsoft.ttorrent.noads", ".AddTorrentActivity")
                || tryStartSpecificActivity(uri, "hu.tagsoft.ttorrent.lite", ".AddTorrentActivity"))
            return;

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(uri);
        startActivity(i);
    }

    private void startAddMagnetActivity(Uri uri) {
        if (tryStartSpecificActivity(uri, "hu.tagsoft.ttorrent.noads", ".AddMagnetActivity")
                || tryStartSpecificActivity(uri, "hu.tagsoft.ttorrent.lite", ".AddMagnetActivity"))
            return;

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(uri);
        startActivity(i);
    }

    private boolean tryStartSpecificActivity(Uri uri, String packageName, String activityName) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setClassName(packageName, activityName);
            i.setData(uri);
            startActivity(i);
            return true;
        }
        catch (ActivityNotFoundException ex) {
            return false;
        }
    }

}
