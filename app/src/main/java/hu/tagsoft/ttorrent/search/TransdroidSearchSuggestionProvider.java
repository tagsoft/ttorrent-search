package hu.tagsoft.ttorrent.search;

import android.content.SearchRecentSuggestionsProvider;

public class TransdroidSearchSuggestionProvider extends
        SearchRecentSuggestionsProvider {

    public final static String AUTHORITY = "hu.tagsoft.ttorrent.search.TransdroidSearchSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public TransdroidSearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
